from pandac.PandaModules import loadPrcFileData

#loadPrcFileData("", "want-directtools #t")
#loadPrcFileData("", "want-tk #t")

loadPrcFileData("", "sync-video #f")
loadPrcFileData("", "show-frame-rate-meter #t")
loadPrcFileData("", "want-pstats 1")

from direct.showbase.ShowBase import ShowBase

from gameCode.shooter import Shooter

class World(ShowBase):

    def __init__(self):
        ShowBase.__init__(self) 
        game = Shooter()
        game.init()

if __name__ == '__main__':
    winst = World()
    winst.run()


