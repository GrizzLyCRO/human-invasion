from pandac.PandaModules import *
from direct.showbase.DirectObject import DirectObject

from gameCode.player import Player
from gameCode.enemy import Enemy

class Shooter(DirectObject):
    def init(self):
        self.loadLevel()
        self.setupCollisionSystem()
        self.loadPlayer()
        
        self.loadLights()
        self.loadEnemies()
        
    def loadPlayer(self):
        self.player = Player()
        self.player.init()
    
    def loadLevel(self):
        self.levelNP = loader.loadModel("models/levels/plain/plain")
        self.levelNP.reparentTo(render)
    
    def loadLights(self):
        alight = AmbientLight('ambientLight')
        alight.setColor(Vec4(0.5, 0.5, 0.5, 1))
        alightNP = render.attachNewNode(alight)

        dlight = DirectionalLight('directionalLight')
        dlight.setDirection(Vec3(1, 1, -1))
        dlight.setColor(Vec4(0.7, 0.7, 0.7, 1))
        dlightNP = render.attachNewNode(dlight)

        render.clearLight()
        render.setLight(alightNP)
        render.setLight(dlightNP)
        
    def loadEnemies(self):
        for i in range(15):
            enemy = Enemy()
            enemy.init()
            
    def setupCollisionSystem(self):

        base.cTrav = CollisionTraverser()
        base.pusher = CollisionHandlerPusher()
        base.handlerEvent = CollisionHandlerEvent()
        base.bullets = render.attachNewNode("bullets")
        
        base.handlerEvent.addInPattern('%fn-into-%in')
        base.handlerEvent.addOutPattern('%fn-out-%in')
        self.accept('bullet-into-enemy', self.handleBulletIntoEnemy)

    def handleBulletIntoEnemy(self, entry):
        bullet = entry.getFromNodePath().getParent().getParent()
        enemy = entry.getIntoNodePath().getParent().getParent()
        x = bullet.getPythonTag("owner")
        x.deleteMe = 1
        y = enemy.getPythonTag("owner")
        z = type(y).__name__
        print z
        if (z != 'NoneType'):
            y.receiveHit(33)
        
        
