from panda3d.core import *
from direct.showbase.DirectObject import DirectObject

class Radar(DirectObject):
    def __init__(self,ship):
        self.radius = 50
        self.detectedNodes = []
        
        cs = CollisionSphere(0, 0, 0, self.radius)
        self.collNodePath = ship.attachNewNode(CollisionNode('radar'))
        self.collNodePath.node().addSolid(cs)
        self.collNodePath.setCompass()
        #self.collNodePath.show()
        fromMask = BitMask32().allOff()
        intoMask = BitMask32().allOff()
        fromMask.setBit(0)
        
        
        self.collNodePath.node().setCollideMask(intoMask)
        self.collNodePath.node().setFromCollideMask(fromMask)
        base.cTrav.addCollider(self.collNodePath, base.handlerEvent)


        self.accept('radar-into-enemy', self.enemyIntoRadar)
        self.accept('radar-out-enemy', self.enemyOutFromRadar)
        
    def enemyIntoRadar(self,entry):
        enemy = entry.getIntoNodePath()
        self.detectedNodes.append(enemy)
    
    def enemyOutFromRadar(self,entry):
        enemy = entry.getIntoNodePath()
        self.detectedNodes.remove(enemy)
        