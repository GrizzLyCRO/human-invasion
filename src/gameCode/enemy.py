import random
from pandac.PandaModules import BitMask32
from direct.gui.DirectGui import *

class Enemy():
        
    def init(self):
        self.enemyNode = render.attachNewNode("enemy")
        self.shipNP = loader.loadModel("models/ships/plimp/plimp2black")
        P = self.shipNP
        P.reparentTo(self.enemyNode)
        P.setPythonTag("owner", self)

        x = random.uniform(-50, 50)
        y = random.uniform(-50, 50)
        P.setPos(x, y, 0)
        self.setupCollision()
        
        self.hp = 300
        self.setupHealthBar()
        taskMgr.add(self.update, "update", uponDeath=self.destroyMe)
    
    def destroyMe(self, x):
        self.shipNP.clearPythonTag("owner")
        self.enemyNode.removeNode()
        
    def update(self, Task):
        if self.hp < 0:
            print "You killed an enemy"
            return Task.done
        else:
            return Task.cont
            
    def receiveHit(self,dmg):
        self.hp -= dmg
        self.hpBar['value'] = self.hp
        
    def setupHealthBar(self):
        self.hpBar = DirectWaitBar(value=50.0, pos=(0, 3, 2))
        self.hpBar.setBin('unsorted', 0)
        self.hpBar.setDepthWrite(False)
        self.hpBar.setLightOff(1)
        self.hpBar.reparentTo(self.shipNP)
        self.hpBar.setScale(2, 1, 2)
        self.hpBar.setHpr(0, 270, 0)
        self.hpBar['value'] = self.hp
        self.hpBar['range'] = self.hp
        self.hpBar['barColor'] = (0.0, 0.9, 0.0, 1.0)
            
    def setupCollision(self):
        fromObject = self.shipNP.find('**/+CollisionNode')
        fromObject.setName("enemy")
        fromMask = BitMask32().allOff()
        intoMask = BitMask32().allOff()
        fromMask.setBit(0)
        fromMask.setBit(3)
        intoMask.setBit(0)
        intoMask.setBit(1)
        intoMask.setBit(2)

        fromObject.node().setFromCollideMask(fromMask)
        fromObject.setCollideMask(intoMask)
        
        base.cTrav.addCollider(fromObject, base.handlerEvent)
        
        base.cTrav.addCollider(fromObject, base.pusher)
        
        base.pusher.addCollider(fromObject, self.shipNP)
