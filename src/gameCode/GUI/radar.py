from panda3d.core import *
from direct.gui.OnscreenImage import OnscreenImage
from direct.showbase.ShowBase import ShowBase

class RadarDisplay():
    
    def __init__(self,player):
        self.player = player
        self.bg=OnscreenImage(image = 'images/radar.png', pos = (0.2, 0, 0.2),scale=(0.2,0,0.2), parent = base.a2dBottomLeft)
        self.bg.setTransparency(TransparencyAttrib.MAlpha)
        self.radarObjects = []
        self.radarScale = 0.95/self.player.radar.radius
        self.dummy = OnscreenImage(image='images/redDot.png',scale=self.radarScale, parent=self.bg)
        self.dummy.setTransparency(TransparencyAttrib.MAlpha)
        
        taskMgr.add(self.redrawRadar,"radarRedraw")
        
        
        
    def redrawRadar(self,task):
        s = self.radarScale
        #clear radar first
        for obj in self.radarObjects:
            obj.removeNode()
        del self.radarObjects[:]
            
        for obj in self.player.radar.detectedNodes:
            pos = obj.getPos(self.player.radar.collNodePath)
            newObj = self.bg.attachNewNode("dot")
            x = self.dummy.copyTo(newObj)
            newObj.reparentTo(self.bg)
            newObj.setPos(pos[0]*s,0,pos[1]*s)
            #newObj.setPos(newObj,pos[0],0,pos[1])
            #newObj.setScale(3,3,3)
            self.radarObjects.append(newObj)
        
        print len(self.radarObjects)
        return task.cont