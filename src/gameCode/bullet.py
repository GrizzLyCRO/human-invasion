from pandac.PandaModules import BitMask32

class Bullet():
    def init(self, type, parent):
        self.parent = parent

        self.bulletNode = render.attachNewNode("bullet")
        self.bulletNP = loader.loadModel("models/bullets/sphere/sphere")
        P = self.bulletNP
        P.reparentTo(self.bulletNode)
        P.setPythonTag("owner", self)
        P.setPos(parent, 0, 1, 0)
        P.setHpr(parent, 0, 0, 0)
        #vars like speed, damage, distance will be passed to some method later
        self.speed = 40.0
        self.distance = 20.0
        self.deleteMe = 0
        self.damage = 12
        #tmaxLife is created var, we need it so bullets dont go on forever
        self.maxLife = self.distance / self.speed
        self.life = 0.00001
        
        taskMgr.add(self.move, "move", uponDeath=self.destroyMe)
        
        self.setupCollision()
        
    def move(self, Task):
        if self.deleteMe == 1:
            return Task.done
        
        elif self.life > self.maxLife:
            return Task.done
        else:
            #move bullet forward, dependant on delta time
            #TODO: rewrite this to use interval, this approach causes
            # too many state changes performance problems
            P = self.bulletNP
            dt = globalClock.getDt()
            P.setY(P, dt * self.speed)
            self.life += dt
            return Task.cont
    
    def destroyMe(self, x):
        self.bulletNP.clearPythonTag("owner")
        print base.cTrav.removeCollider(self.bulletCollisionNode)
        self.bulletNode.removeNode()
        
    def setupCollision(self):
        fromObject = self.bulletNP.find('**/+CollisionNode')
        fromObject.setName("bullet")

        fromMask = BitMask32().allOff()
        intoMask = BitMask32().allOff()
        fromMask.setBit(1)

        
        fromObject.node().setFromCollideMask(fromMask)
        fromObject.setCollideMask(intoMask)
        self.bulletCollisionNode = fromObject
        base.cTrav.addCollider(fromObject, base.handlerEvent)
