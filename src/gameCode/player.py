from direct.showbase.InputStateGlobal import inputState
from pandac.PandaModules import BitMask32
from gameCode.bullet import Bullet
from gameCode.radar import Radar
from gameCode.GUI.radar import RadarDisplay

class Player():
    
    def init(self):
        self.setupShip()
        self.setupCamera()
        self.setupControls()
        #self.setupGUI()
        self.lastShotFired = 0.0
        self.aspectRatio = 1.333
    
        
    def setupShip(self):
        playerNode = render.attachNewNode("player")
        self.shipNP = loader.loadModel("models/ships/plimp/plimp2")
        self.shipNP.reparentTo(playerNode)
        self.setupCollisions()
        #self.setupRadar()
        self.mountWeapons()
        
    def setupCollisions(self):
        
        self.shipNP.setName("shipNP")
        fromObject = self.shipNP.find('**/+CollisionNode')
        
        fromObject.setName("player")
        
        fromMask = BitMask32().allOff()
        intoMask = BitMask32().allOff()
        fromMask.setBit(0)
        fromMask.setBit(3)
        intoMask.setBit(0)
        intoMask.setBit(1)
        intoMask.setBit(2)
        
        fromObject.node().setFromCollideMask(fromMask)
        fromObject.setCollideMask(intoMask)
        
        base.cTrav.addCollider(fromObject, base.handlerEvent)
        
        base.cTrav.addCollider(fromObject, base.pusher)
        
        base.pusher.addCollider(fromObject, self.shipNP)
    
    def setupCamera(self):
        #reparent camera to player
        base.cam.reparentTo(self.shipNP)
        base.cam.setPos(0, 0, 100)
        base.cam.lookAt(self.shipNP)
        base.cam.setCompass()
        
        
    def setupControls(self):
        
        #Call to task which will align ship to mouse cursor
        taskMgr.add(self.rotateShip, "rotate ship")
        
        #handle input
        taskMgr.add(self.processInput, "process input")
        
        #keyboard shortcuts
        inputState.watchWithModifiers('forward', 'w')
        inputState.watchWithModifiers('left', 'a')
        inputState.watchWithModifiers('reverse', 's')
        inputState.watchWithModifiers('right', 'd')
        inputState.watchWithModifiers('fire primary', 'space')
    
    def firePrimary(self):
        fireRate = 100.0 #bullets per second
        if self.lastShotFired > 1.0 / fireRate:
            self.lastShotFired = 0.0
            for weap in self.weapons:
                b = Bullet()
                b.init("sphere", weap)

    
    def processInput(self, Task):
        self.dt = globalClock.getDt()
        P = self.shipNP
        dt = self.dt
        #this is temp hack until we have full RPG system
        speed = 40 #units per second (because we multiply it by dt, which is lower than 1)
        
        #This looks a bit hacky, but i think its proper way of writing it
        #we check if user pressed fire to start task which will fire
        self.lastShotFired += self.dt
        
        if inputState.isSet('fire primary'):
            self.firePrimary()
                
        
        #BEGIN Movement code
        #ship movement mode, can be "relative" or "absolute"
        modeY = "relative"
        modeX = "relative"
        
        if inputState.isSet('forward'):
            if modeY == "absolute":
                #set Y coordinate of ship (P) relative to render node
                P.setY(render, P.getY() + dt * speed)
            else:
                P.setY(P, dt * speed)
                
        if inputState.isSet('reverse'):
            if modeY == "absolute":
                P.setY(render, P.getY() - dt * speed)
            else:
                P.setY(P, -dt * speed)
                
        if inputState.isSet('left'):
            if modeX == "absolute":
                P.setX(render, P.getX() - dt * speed)
            else:
                P.setX(P, -dt * speed)
                        
        if inputState.isSet('right'):
            if modeX == "absolute":
                P.setX(render, P.getX() + dt * speed)
            else:
                P.setX(P, dt * speed)
        #END Movement code
            
        return Task.cont 
    
    def rotateShip(self, Task):
        if base.mouseWatcherNode.hasMouse():
            #get the mouse position as a Vec2. The values for each axis are from -1 to
            #1. The top-left is (-1,-1), the bottom right is (1,1)
            mpos = base.mouseWatcherNode.getMouse()
            P = self.shipNP
            bug = 1000000
            #TODO add real read
            P.lookAt(bug * mpos[0] * self.aspectRatio, bug * mpos[1], 0)
            
        return Task.cont 
    
    def setupRadar(self):
        self.radar = Radar(self.shipNP)
        
    def setupGUI(self):
        self.radarDisplay = RadarDisplay(self)
        
    def mountWeapons(self):
        self.weapons = []
        mountPoints = self.shipNP.findAllMatches("**/=type=weaponMount")
        for point in mountPoints:
            weapon = loader.loadModel("models/weapons/metnemti")
            weapon.reparentTo(point)
            self.weapons.append(weapon)
        
    
        
